//
//  ErrorModel.swift
//  MeetLara
//
//  Created by Ernesto Jose Contreras Lopez on 3/11/23.
//

import Foundation

struct BaseError: Error {
    let message: String
    let code: Int
    
    init(error: Localized.API.Error, message: String = "") {
        self.message = "\(error.string). \(message)"
        self.code = error.code
    }
}
