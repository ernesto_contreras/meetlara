//
//  UserModel.swift
//  MeetLara
//
//  Created by Ernesto Jose Contreras Lopez on 3/11/23.
//

import Foundation

struct User: Codable, DictionaryConvertible {
    var full_name: String
    var email: String
    var instagram: String
}
