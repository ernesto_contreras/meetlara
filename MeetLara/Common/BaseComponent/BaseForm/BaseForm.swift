//
//  BaseForm.swift
//  MeetLara
//
//  Created by Ernesto Jose Contreras Lopez on 30/10/23.
//

import UIKit

class BaseForm: BaseView {
    // MARK: - Layout properties
    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var contentStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 4
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 22, weight: .bold)
        return label
    }()
    
    private lazy var textFieldContainer: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.layer.cornerRadius = 10
        view.layer.borderWidth = 1
        view.layer.borderColor = getTheme().color.cgColor
        return view
    }()
    
    private lazy var hStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.spacing = 0
        return stackView
    }()

    private lazy var imageViewContainer: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    private var imageView: UIImageView = {
        let image = Localized.SystemImages.xmark_circle_fill.image
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFit
        imageView.tintColor = Localized.Colors.contrast.color
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12, weight: .light)
        return label
    }()
    
    private lazy var errorLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12, weight: .light)
        label.textColor = .red
        return label
    }()
    
    private lazy var passwordRevealButton: UIButton = {
        let button = UIButton()
        button.setImage(Localized.Images.eye_icon.image, for: .normal)
        button.setImage(Localized.Images.eye_off_icon.image, for: .selected)
        button.tintColor = Localized.Colors.contrast.color
        button.addTarget(self, action: #selector(pwdRevealBtnTapped(_:)), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.isHidden = true
        return button
    }()
    
    private lazy var imageHStackView: UIStackView = {
        let imageHStackView = UIStackView()
        imageHStackView.axis = .horizontal
        imageHStackView.distribution = .fillProportionally
        imageHStackView.spacing = 8
        imageHStackView.translatesAutoresizingMaskIntoConstraints = false
        return imageHStackView
    }()
    
    public lazy var textField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .clear
        textField.font = UIFont.systemFont(ofSize: 16, weight: .light)
        textField.autocorrectionType = .no
        textField.textColor = Localized.Colors.primaryText.color
        let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 16, height: 0))
        textField.leftView = paddingView
        textField.rightView = paddingView
        textField.rightViewMode = .always
        textField.leftViewMode = .always

        return textField
    }()
    private var minLenghtView = FormCheckView()
    private var uppercaseView = FormCheckView()
    private var specialCharView = FormCheckView()
    private var numberView = FormCheckView()
    private var passwordsMatchView = FormCheckView()
    
    // MARK: - Section type
    enum FormType: Equatable {
        case regular
        case withError
        case password
        case success
        case email
    }
    
    // MARK: - Properties
    private var title: String?
    private var placeholder: String?
    private var descriptionText: String?
    private var errorText: String?
    private var hasSetupCompleted: Bool = false
    private var hasPasswordChecklist: Bool!
    private var hasPasswordCheck: Bool!
    private var initialType: FormType!
    private var type: FormType = .regular {
        didSet {
            updateUI()
        }
    }
    private var updatedText: String?
    var validation: Bool = true
    
    // MARK: - Setup
    override func layoutSubviews() {
        super.layoutSubviews()
        if !hasSetupCompleted {
            setup()
            hasSetupCompleted = true
        }
    }
    
    func setup(title: String,
               placeholder: String? = nil,
               description: String? = nil,
               error: String? = nil,
               type: FormType = .regular,
               isNumber: Bool = false,
               isDisabled: Bool = false,
               hasPasswordChecklist: Bool = false,
               hasPasswordCheck: Bool = false) {
        self.title = title
        self.placeholder = placeholder
        self.descriptionText = description
        self.type = type
        self.errorText = error
        self.hasPasswordChecklist = hasPasswordChecklist
        self.hasPasswordCheck = hasPasswordCheck
        self.initialType = type
        self.textField.delegate = self
        self.textField.isUserInteractionEnabled = !isDisabled
        self.textField.keyboardType = isNumber ? .numberPad : .default
        self.setTheme(.translucent)
        self.updateUI()
    }

    private func setup() {
        setupUI()
    }
    
    private func setupUI() {
        addSubview(containerView)
        containerView.addSubview(contentStackView)
        containerView.pinToEdges(of: self)
        
        contentStackView.addArrangedSubview(titleLabel)
        contentStackView.addArrangedSubview(textFieldContainer)
        contentStackView.addArrangedSubview(descriptionLabel)
        contentStackView.addArrangedSubview(errorLabel)
        
        textFieldContainer.addSubview(hStackView)
        hStackView.addArrangedSubview(textField)
        hStackView.addArrangedSubview(imageViewContainer)
        imageViewContainer.addSubview(imageHStackView)
        imageHStackView.addArrangedSubview(passwordRevealButton)
        imageHStackView.addArrangedSubview(imageView)
        
        backgroundColor = .clear
        descriptionLabel.textColor = getTheme().color
        containerView.backgroundColor = getTheme().bgColor
        titleLabel.textColor = getTheme().color
        
        NSLayoutConstraint.activate([
            contentStackView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 8),
            contentStackView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            contentStackView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            contentStackView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor),
            
            hStackView.topAnchor.constraint(equalTo: textFieldContainer.topAnchor, constant: 4),
            hStackView.leadingAnchor.constraint(equalTo: textFieldContainer.leadingAnchor),
            hStackView.trailingAnchor.constraint(equalTo: textFieldContainer.trailingAnchor, constant: -12),
            hStackView.bottomAnchor.constraint(equalTo: textFieldContainer.bottomAnchor, constant: -4),
            textField.heightAnchor.constraint(equalToConstant: 40),

            imageHStackView.centerYAnchor.constraint(equalTo: imageViewContainer.centerYAnchor),
            imageHStackView.leadingAnchor.constraint(equalTo: imageViewContainer.leadingAnchor),
            imageHStackView.trailingAnchor.constraint(equalTo: imageViewContainer.trailingAnchor),
            imageHStackView.heightAnchor.constraint(equalToConstant: 26),
            passwordRevealButton.widthAnchor.constraint(equalToConstant: 26),
            imageView.widthAnchor.constraint(equalToConstant: 20)
        ])
        setupAttributedPlaceholder()
        setupPasswordValidations()
    }
    
    private func setupAttributedPlaceholder() {
        if let placeholder = self.placeholder {
            textField.attributedPlaceholder = NSAttributedString(
                string: placeholder,
                attributes: [.foregroundColor: Localized.Colors.secondaryText.color]
            )
        }
    }

    private func setupPasswordValidations() {
        let validationStackView = UIStackView()
        validationStackView.axis = .vertical
        validationStackView.spacing = 8
        
        let separatorView = UIView()
        separatorView.translatesAutoresizingMaskIntoConstraints = false
        separatorView.backgroundColor = .clear
        separatorView.heightAnchor.constraint(equalToConstant: 4).isActive = true
        
        if let hasChecklist = self.hasPasswordChecklist, hasChecklist {
            minLenghtView.setup(type: .minLenght)
            uppercaseView.setup(type: .uppercase)
            specialCharView.setup(type: .specialCharacter)
            numberView.setup(type: .number)
            
            validationStackView.addArrangedSubview(separatorView)
            validationStackView.addArrangedSubview(minLenghtView)
            validationStackView.addArrangedSubview(uppercaseView)
            validationStackView.addArrangedSubview(specialCharView)
            validationStackView.addArrangedSubview(numberView)
            
            let validationContainerStackView = UIStackView()
            validationStackView.axis = .vertical
            validationStackView.spacing = 8
            
            let leftPaddingView = UIView()
            leftPaddingView.translatesAutoresizingMaskIntoConstraints = false
            leftPaddingView.backgroundColor = .clear
            leftPaddingView.widthAnchor.constraint(equalToConstant: 8).isActive = true
            
            validationContainerStackView.addArrangedSubview(leftPaddingView)
            validationContainerStackView.addArrangedSubview(validationStackView)
            contentStackView.addArrangedSubview(validationContainerStackView)
        } else if let hasCheck = self.hasPasswordCheck, hasCheck {
            passwordsMatchView.setup(type: .passwordsMatch)
            
            validationStackView.addArrangedSubview(separatorView)
            validationStackView.addArrangedSubview(passwordsMatchView)
            contentStackView.addArrangedSubview(validationStackView)
        }
    }
    
    private func updateUI() {
        titleLabel.text = title
        descriptionLabel.text = descriptionText
       
        switch type {
        case .regular:
            textField.returnKeyType = .next
            imageViewContainer.isHidden = true
            resetUI()
        case .password:
            textField.isSecureTextEntry = true
            textField.returnKeyType = .done
            imageViewContainer.isHidden = false
            passwordRevealButton.isHidden = false
            imageView.isHidden = true
            resetUI()
        case .success:
            imageViewContainer.isHidden = false
            imageView.isHidden = false
            imageView.image = Localized.Images.check_circle.image
            resetUI()
        case .withError:
            errorLabel.text = errorText
            textFieldContainer.layer.borderColor = UIColor.red.cgColor
            imageViewContainer.isHidden = true
            imageView.isHidden = true
        case .email:
            textField.autocapitalizationType = .none
            textField.keyboardType = .emailAddress
            textField.returnKeyType = .done
            imageViewContainer.isHidden = true
            resetUI()
        }
        descriptionLabel.isHidden = descriptionLabel.text?.isEmpty ?? true
        errorLabel.isHidden = errorLabel.text?.isEmpty ?? true
    }
    
    private func resetUI() {
        textFieldContainer.layer.borderColor = getTheme().color.cgColor
        errorLabel.text = nil
    }
    
    private func focusForm(should: Bool = true) {
        if should {
            textFieldContainer.layer.borderColor = Localized.Colors.contrast.color.cgColor
        } else {
            textFieldContainer.layer.borderColor = getTheme().color.cgColor
        }
    }
    
    @objc private func pwdRevealBtnTapped(_ sender: UIButton) {
        sender.isSelected.toggle()
        textField.isSecureTextEntry.toggle()
    }
}

extension BaseForm {
    public func content() -> String {
        self.updatedText ?? ""
    }
    
    public func isValid() -> Bool {
        switch type {
        case .email:
            return content().isValidEmail()
        default:
            return !content().isEmpty
        }
    }
    
    public func hasValue() -> Bool {
        !(self.textField.text?.isEmpty ?? true)
    }
    
    public func enableForm(should: Bool = true) {
        self.textField.isUserInteractionEnabled = should
        self.textField.text = nil
    }
    
    public func updateType(_ type: FormType, andInitialType: Bool = false) {
        self.type = type
        if andInitialType {
            self.initialType = type
        }
    }
    
    public func updateDescription(_ text: String) {
        self.descriptionText = text
        self.descriptionLabel.text = text
    }
    
    public func updateText(_ text: String?) {
        self.textField.text = text
    }
    
    public func updateError(_ text: String) {
        self.errorText = text
        self.errorLabel.text = text
    }
    
    public func updateImage(_ image: UIImage) {
        self.imageView.image = image
    }
    
    public func updatePasswordValidation(_ type: ValidationType, validation: Bool) {
        switch type {
        case .minLenght:
            self.minLenghtView.update(with: validation)
        case .uppercase:
            self.uppercaseView.update(with: validation)
        case .number:
            self.numberView.update(with: validation)
        case .specialCharacter:
            self.specialCharView.update(with: validation)
        case .passwordsMatch:
            self.passwordsMatchView.update(with: validation)
        }
    }
    
    public func hasPasswordValidations() -> Bool {
        minLenghtView.isValid() &&
        uppercaseView.isValid() &&
        numberView.isValid() &&
        specialCharView.isValid()
    }
    
    public func resetPasswordValidations() {
        self.minLenghtView.update(with: false)
        self.uppercaseView.update(with: false)
        self.numberView.update(with: false)
        self.specialCharView.update(with: false)
        self.passwordsMatchView.update(with: false)
    }
}

extension BaseForm: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        NotificationCenter.default.post(name: .didStartEditingForm, object: self)
        focusForm()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let updatedText = textField.getUpdatedText(from: range, with: string)
        self.updatedText = updatedText
        
        if initialType == .password, string == "" {
            self.updatedText = nil
            NotificationCenter.default.post(name: .didFinishEditingForm, object: textField)
        }
        
        NotificationCenter.default.post(name: .didUpdateForm, object: textField)
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        NotificationCenter.default.post(name: .didFinishEditingForm, object: textField)
        focusForm(should: false)
        switch initialType {
        case .email:
            if let text = textField.text {
                self.updateType(text.isValidEmail() ? .success : .withError)
            } else {
                self.updateType(.email)
            }
        default:
            if let text = textField.text, initialType != .password {
                self.updateType(!text.isEmpty ? .success : .withError)
            } else {
                self.updateType(initialType)
            }
        }
    }
}
