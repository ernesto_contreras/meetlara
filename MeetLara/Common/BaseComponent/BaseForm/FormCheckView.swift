//
//  FormCheckView.swift
//  MeetLara
//
//  Created by Ernesto Jose Contreras Lopez on 30/10/23.
//

import UIKit

enum ValidationType {
    case minLenght
    case uppercase
    case number
    case specialCharacter
    case passwordsMatch
}

class FormCheckView: UIView {
    private let checkImageView: UIImageView = {
        let imageView = Localized.Images.check_circle.imageView
        imageView.tintColor = Localized.Colors.secondaryText.color.withAlphaComponent(0.7)
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    private let descriptionLabel: UILabel = {
        let label = UILabel()
//        label.font = UIFont(name: "Manrope-Regular", size: 12)
        label.textColor = Localized.Colors.secondaryText.color.withAlphaComponent(0.7)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private var validation: Bool = false {
        didSet {
            self.updateValidation()
        }
    }
    private var descriptionText: String!
    private var hasSetupCompleted: Bool = false
    private var type: ValidationType!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if !hasSetupCompleted {
            setupUI()
        }
    }
    
    private func setupUI() {
        hasSetupCompleted = true
        translatesAutoresizingMaskIntoConstraints = false
        setupStackView()
        setupLabel()
    }
    
    private func setupStackView() {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.spacing = 8
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.addArrangedSubview(checkImageView)
        stackView.addArrangedSubview(descriptionLabel)
        self.addSubview(stackView)
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: self.topAnchor),
            stackView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            stackView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            checkImageView.heightAnchor.constraint(equalToConstant: 20),
            checkImageView.widthAnchor.constraint(equalToConstant: 20)
        ])
    }
    
    private func setupLabel() {
        switch type {
        case .minLenght:
            self.descriptionLabel.text = Localized.Texts.lengthLimit.string
        case .number:
            self.descriptionLabel.text = Localized.Texts.numberLimit.string
        case .specialCharacter:
            self.descriptionLabel.text = Localized.Texts.characterLimit.string
        case .uppercase:
            self.descriptionLabel.text = Localized.Texts.capitalLimit.string
        case .passwordsMatch:
            self.descriptionLabel.text = Localized.Texts.matchPasswords.string
        default:
            break
        }
    }
    
    private func updateValidation() {
        if validation {
            checkImageView.tintColor = Localized.Colors.contrast.color
            descriptionLabel.textColor = Localized.Colors.primaryText.color
        } else {
            checkImageView.tintColor = Localized.Colors.secondaryText.color.withAlphaComponent(0.7)
            descriptionLabel.textColor = Localized.Colors.secondaryText.color.withAlphaComponent(0.7)
        }
    }
    
    public func setup(type: ValidationType) {
        self.type = type
    }
    
    public func update(with validation: Bool) {
        self.validation = validation
    }
    
    public func isValid() -> Bool {
        self.validation
    }
}


