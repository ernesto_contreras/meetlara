//
//  BaseView.swift
//  MeetLara
//
//  Created by Ernesto Jose Contreras Lopez on 30/10/23.
//

import UIKit

class BaseView: UIView {
    // MARK: - Theme
    enum ThemeType {
        case light
        case dark
        case translucent
        
        var bgColor: UIColor {
            switch self {
            case .light:
                return Localized.Colors.secondary.color
            case .dark:
                return Localized.Colors.primary.color
            case .translucent:
                return .clear
            }
        }
        
        var color: UIColor {
            switch self {
            case .light, .translucent:
                return Localized.Colors.primaryText.color
            case .dark:
                return Localized.Colors.secondary.color
            }
        }
    }
    
    // MARK: - Properties
    private let activityIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView(style: .medium)
        indicator.hidesWhenStopped = true
        indicator.translatesAutoresizingMaskIntoConstraints = false
        return indicator
    }()
    private var theme: ThemeType = .light {
        didSet {
            setupColors()
        }
    }
    private var centeredLoading: Bool = true
    
    // MARK: - Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupUI()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setupConstraints()
    }
    
    // MARK: - Private
    private func setupUI() {
        self.translatesAutoresizingMaskIntoConstraints = false
        addSubview(activityIndicator)
        setupColors()
    }
    
    private func setupColors() {
        backgroundColor = theme.bgColor
        activityIndicator.color = theme.color
    }
    
    private func setupConstraints() {
        activityIndicator.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        if centeredLoading {
            activityIndicator.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        } else {
            activityIndicator.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -24).isActive = true
        }
    }
    
    // MARK: - Public
    public func getTheme() -> ThemeType {
        return theme
    }
    
    public func setTheme(_ theme: ThemeType) {
        self.theme = theme
    }
    
    public func setupLoading(centered: Bool = true, style: UIActivityIndicatorView.Style = .large) {
        self.centeredLoading = centered
        self.activityIndicator.style = style
    }
    
    public func showLoading() {
        DispatchQueue.main.async {
            self.activityIndicator.startAnimating()
        }
    }
    
    public func hideLoading() {
        DispatchQueue.main.async {        
            self.activityIndicator.stopAnimating()
        }
    }
}
