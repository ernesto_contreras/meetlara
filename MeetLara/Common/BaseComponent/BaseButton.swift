//
//  BaseButton.swift
//  MeetLara
//
//  Created by Ernesto Jose Contreras Lopez on 30/10/23.
//

import UIKit

class BaseButton: BaseView {
    enum ButtonType {
        case filled
        case reversed
        case outlined
        case borderless
    }
    
    // MARK: - Properties
    private var type: ButtonType = .filled
    private var hasSetupCompleted: Bool = false
    private lazy var button: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        return button
    }()
    
    // MARK: - Lifecycle
    override func layoutSubviews() {
        super.layoutSubviews()
        if !hasSetupCompleted {
            setupButtonUI()
            hasSetupCompleted = true
        }
    }
    
    init(type: ButtonType = .filled, theme: ThemeType = .dark, title: String,
         target: Any?, action: Selector) {
        super.init(frame: .zero)
        self.type = type
        setTheme(theme)
        button.setTitle(title, for: .normal)
        setupLoading(centered: false, style: .medium)
        button.addTarget(target, action: action, for: .touchUpInside)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupButtonUI() {
        addSubview(button)
        self.clipsToBounds = true
        self.layer.cornerRadius = 12
        switch type {
        case .filled:
            self.backgroundColor = getTheme().bgColor
            button.setTitleColor(getTheme().color, for: .normal)
        case .reversed:
            self.backgroundColor = getTheme().color
            button.setTitleColor(getTheme().bgColor, for: .normal)
        case .outlined:
            self.backgroundColor = .clear
            self.layer.borderColor = getTheme().bgColor.cgColor
            self.layer.borderWidth = 1
            button.setTitleColor(getTheme().bgColor, for: .normal)
        case .borderless:
            self.backgroundColor = .clear
            button.setTitleColor(getTheme().color, for: .normal)
        }
        setupConstraints()
    }

    // MARK: - Private
    private func setupConstraints() {
        button.pinToEdges(of: self)
        NSLayoutConstraint.activate([
            button.heightAnchor.constraint(equalToConstant: 48)
        ])
    }
}
