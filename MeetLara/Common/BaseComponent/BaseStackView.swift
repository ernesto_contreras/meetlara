//
//  BaseStackView.swift
//  MeetLara
//
//  Created by Ernesto Jose Contreras Lopez on 2/11/23.
//

import UIKit

class BaseStackView: UIStackView {
    init(space: CGFloat, orientation: NSLayoutConstraint.Axis, subViews: [UIView]? = nil) {
        super.init(frame: .zero)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.axis = orientation
        self.spacing = space
        if let arrangedSubViews = subViews {
            for view in arrangedSubViews {
                self.addArrangedSubview(view)
            }
        }
    }

    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
