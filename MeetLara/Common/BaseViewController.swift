//
//  BaseViewController.swift
//  MeetLara
//
//  Created by Ernesto Jose Contreras Lopez on 30/10/23.
//

import UIKit

class BaseViewController: UIViewController {
    // MARK: - UI Properties
    public lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.showsVerticalScrollIndicator = false
        return scrollView
    }()
    public lazy var contentView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = Localized.Colors.secondary.color
        view.layer.cornerRadius = 40
        view.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        view.layer.masksToBounds = true
        return view
    }()
    public lazy var bottomBackgroundView: UIView = {
        let bgView = UIView()
        bgView.translatesAutoresizingMaskIntoConstraints = false
        bgView.backgroundColor = Localized.Colors.secondary.color
        return bgView
    }()
    private var loadingView: UIView = {
        let loadingView = UIView(frame: .zero)
        loadingView.backgroundColor = UIColor.white.withAlphaComponent(0.8)
        loadingView.isHidden = true
        loadingView.translatesAutoresizingMaskIntoConstraints = false
        return loadingView
    }()
    private var loadingImageView: UIImageView = {
        let loadingImageView = Localized.Images.loading_indicator.imageView
        loadingImageView.translatesAutoresizingMaskIntoConstraints = false
        return loadingImageView
    }()
    private var messageLabel: UILabel = {
        let messageLabel = UILabel()
        messageLabel.text = Localized.Texts.loadingMessage.string
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        messageLabel.textColor = .white
        messageLabel.textAlignment = .center
//        messageLabel.font = UIFont(name: "Manrope-Regular", size: 18)
        messageLabel.numberOfLines = 2
        return messageLabel
    }()
    private var containerStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 24
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    // MARK: - Properties
    private var isLoading: Bool = false
    public var selectedView: UIView?
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupObservers()
        setupUI()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        setupTopBar()
    }
    
    // MARK: - Setup
    private func setupObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    private func setupUI() {
        setupView()
        setupScrollView()
    }
    
    private func setupTopBar() {
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        if let statusBarFrame = view.window?.windowScene?.statusBarManager?.statusBarFrame {
            let statusBarView = UIView(frame: statusBarFrame)
            statusBarView.autoresizingMask = [.flexibleWidth, .flexibleTopMargin]
            statusBarView.backgroundColor = Localized.Colors.primary.color
            view.addSubview(statusBarView)
        }
    }
    
    private func setupView() {
        view.backgroundColor = Localized.Colors.primary.color
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard)))
        view.addSubview(bottomBackgroundView)
        view.sendSubviewToBack(bottomBackgroundView)
        NSLayoutConstraint.activate([
            bottomBackgroundView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            bottomBackgroundView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            bottomBackgroundView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            bottomBackgroundView.heightAnchor.constraint(equalToConstant: view.bounds.height / 2)
        ])
    }
    
    private func setupScrollView() {
        view.addSubview(scrollView)
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor)])
        scrollView.addSubview(contentView)
        contentView.pinToEdges(of: scrollView)
    }
    
    private func setupLoading() {
        view.addSubview(loadingView)
        loadingView.addSubview(containerStackView)
        let loadingContainerView = UIView()
        loadingContainerView.backgroundColor = .clear
        loadingContainerView.translatesAutoresizingMaskIntoConstraints = false
        loadingView.addSubview(loadingContainerView)
        loadingContainerView.addSubview(loadingImageView)
        containerStackView.addArrangedSubview(loadingContainerView)
        containerStackView.addArrangedSubview(messageLabel)
        NSLayoutConstraint.activate([
            loadingView.topAnchor.constraint(equalTo: self.view.topAnchor),
            loadingView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
            loadingView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            loadingView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            loadingImageView.widthAnchor.constraint(equalToConstant: 100),
            loadingImageView.heightAnchor.constraint(equalToConstant: 100),
            loadingImageView.topAnchor.constraint(equalTo: loadingContainerView.topAnchor),
            loadingImageView.bottomAnchor.constraint(equalTo: loadingContainerView.bottomAnchor),
            loadingImageView.centerXAnchor.constraint(equalTo: loadingContainerView.centerXAnchor),
            containerStackView.leadingAnchor.constraint(equalTo: loadingView.leadingAnchor, constant: 48),
            containerStackView.trailingAnchor.constraint(equalTo: loadingView.trailingAnchor, constant: -48),
            containerStackView.centerXAnchor.constraint(equalTo: loadingView.centerXAnchor),
            containerStackView.centerYAnchor.constraint(equalTo: loadingView.centerYAnchor)
        ])
    }

    // MARK: - Actions
    @objc private func keyboardWillShow(_ notification: Notification) {
        guard let userInfo = notification.userInfo,
              let keyboardFrame = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect else { return }
        let keyboardHeight = keyboardFrame.size.height
        let contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardHeight, right: 0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
    
    @objc private func keyboardWillHide(_ notification: Notification) {
        scrollView.contentInset = .zero
        scrollView.scrollIndicatorInsets = .zero
    }
}

// MARK: - Loading implementation
extension BaseViewController {
    // MARK: - Public
    @objc public func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    @objc public func startLoading() {
        DispatchQueue.main.async {
            self.setupLoading()
            self.loadingView.isHidden = false
            self.loadingView.alpha = 1
            let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
            rotateAnimation.fromValue = 0.0
            rotateAnimation.toValue = CGFloat(.pi * 2.0)
            rotateAnimation.duration = 0.9
            rotateAnimation.repeatCount = Float.greatestFiniteMagnitude
            if !self.isLoading {
                self.loadingImageView.layer.add(rotateAnimation, forKey: "rotate")
                self.isLoading = true
            }
        }
    }
    
    @objc public func stopLoading() {
        DispatchQueue.main.async {
            self.loadingImageView.layer.removeAnimation(forKey: "rotate")
            self.isLoading = false
            UIView.animate(withDuration: 0.2, delay: 0.0) {
                self.loadingView.alpha = 0
            } completion: { _ in
                self.loadingView.isHidden = true
                self.loadingView.removeFromSuperview()
            }
        }
    }
}
