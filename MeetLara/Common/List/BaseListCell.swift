//
//  BaseListCell.swift
//  MeetLara
//
//  Created by Ernesto Jose Contreras Lopez on 30/10/23.
//

import UIKit

class BaseListCell: UITableViewCell {
    static let identifier = String(describing: BaseListCell.self)
    
    lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.masksToBounds = true
        view.layer.cornerRadius = 12
        return view
    }()
    
    lazy var contentHorizontalStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.spacing = 12
        return stackView
    }()
    
    lazy var contentVerticalStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 8
        return stackView
    }()
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.backgroundColor = .clear
//        label.font = UIFont(name: "Manrope-Regular", size: 16)
        label.textAlignment = .left
        return label
    }()
    
    lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.lightGray
        label.numberOfLines = 0
        label.backgroundColor = .clear
//        label.font = UIFont(name: "Manrope-Regular", size: 12)
        label.textAlignment = .left
        return label
    }()
    
    lazy var leftContainerView: UIView = {
        let iconView = UIView()
        iconView.translatesAutoresizingMaskIntoConstraints = false
        iconView.backgroundColor = .clear
        return iconView
    }()
    
    lazy var leftIconImageView: UIImageView = {
        let icon = UIImageView()
        icon.translatesAutoresizingMaskIntoConstraints = false
        icon.contentMode = .scaleAspectFit
        icon.tintColor = Localized.Colors.contrast.color
        return icon
    }()
    
    lazy var rightContainerView: UIView = {
        let iconView = UIView()
        iconView.translatesAutoresizingMaskIntoConstraints = false
        iconView.backgroundColor = .clear
        return iconView
    }()
    
    lazy var rightIconImageView: UIImageView = {
        let icon = UIImageView()
        icon.translatesAutoresizingMaskIntoConstraints = false
        icon.contentMode = .scaleAspectFit
        icon.tintColor = Localized.Colors.contrast.color
        return icon
    }()
    
    override func prepareForReuse() {
        super.prepareForReuse()
        setupUI()
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .gray
        selectionStyle = .none
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(with data: ListableItem) {
        titleLabel.text = data.title
        subtitleLabel.text = data.subtitle
        subtitleLabel.isHidden = data.subtitle.isNilOrEmpty
        
        switch data.type {
        case .regular:
            if let icon = data.getRightIcon() {
                rightIconImageView.image = icon
            } else {
                rightContainerView.isHidden = true
            }
            if let icon = data.getLeftIcon() {
                leftIconImageView.image = icon
            } else {
                leftContainerView.isHidden = true
            }
        case .radio:
            updateRadio(data.isSelected)
        case .check:
            rightIconImageView.image = Localized.Images.unchecked_square.image
            leftContainerView.isHidden = true
        }
    }
    
    private func updateRadio(_ should: Bool) {
        if should {
            rightIconImageView.image = Localized.Images.radio_on.image
            rightIconImageView.tintColor = .white
            containerView.backgroundColor = .black
            titleLabel.textColor = .white
        } else {
            rightIconImageView.image = Localized.Images.radio_off.image
            rightIconImageView.tintColor = .black
            containerView.backgroundColor = .white
            titleLabel.textColor = .lightGray
        }
        leftContainerView.isHidden = true
    }
    
    private func setSelectedCell(isSelectedCell: Bool) {
        let color: UIColor = isSelectedCell ? .black : UIColor.white
        let textColor: UIColor = isSelectedCell ? .white : .lightGray
        containerView.backgroundColor = color
        titleLabel.textColor = textColor
    }
    
    private func setupUI() {
        setupConstraints()
    }
    
    private func setupConstraints() {
        addSubview(containerView)
        containerView.addSubview(contentHorizontalStackView)
        
        contentHorizontalStackView.addArrangedSubview(leftContainerView)
        leftContainerView.addSubview(leftIconImageView)
        
        contentHorizontalStackView.addArrangedSubview(contentVerticalStackView)
        contentVerticalStackView.addArrangedSubview(titleLabel)
        contentVerticalStackView.addArrangedSubview(subtitleLabel)

        contentHorizontalStackView.addArrangedSubview(rightContainerView)
        rightContainerView.addSubview(rightIconImageView)

        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 8),
            containerView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8),
            containerView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8),
            containerView.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            contentHorizontalStackView.topAnchor.constraint(equalTo: containerView.topAnchor),
            contentHorizontalStackView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 12),
            contentHorizontalStackView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -12),
            contentHorizontalStackView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor),
            
            leftIconImageView.widthAnchor.constraint(equalToConstant: 24),
            leftIconImageView.heightAnchor.constraint(equalToConstant: 24),
            leftIconImageView.centerYAnchor.constraint(equalTo: leftContainerView.centerYAnchor),
            leftIconImageView.leadingAnchor.constraint(equalTo: leftContainerView.leadingAnchor),
            leftIconImageView.trailingAnchor.constraint(equalTo: leftContainerView.trailingAnchor),
            
            rightIconImageView.widthAnchor.constraint(equalToConstant: 24),
            rightIconImageView.heightAnchor.constraint(equalToConstant: 24),
            rightIconImageView.centerYAnchor.constraint(equalTo: rightContainerView.centerYAnchor),
            rightIconImageView.leadingAnchor.constraint(equalTo: rightContainerView.leadingAnchor),
            rightIconImageView.trailingAnchor.constraint(equalTo: rightContainerView.trailingAnchor)
        ])
    }
}
