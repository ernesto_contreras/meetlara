//
//  BaseListViewController.swift
//  MeetLara
//
//  Created by Ernesto Jose Contreras Lopez on 30/10/23.
//

import UIKit

struct ListableItem {
    var title: String
    var type: ListType = .regular
    var content: String?
    var subtitle: String?
    var rightIcon: String?
    var leftIcon: String?
    var isSelected: Bool = false
    
    enum ListType {
        case check
        case radio
        case regular
    }
    
    func getRightIcon() -> UIImage? {
        if let rightIcon = self.rightIcon {
            return UIImage(named: rightIcon)
        }
        return nil
    }
    
    func getLeftIcon() -> UIImage? {
        if let leftIcon = self.leftIcon {
            return UIImage(named: leftIcon)
        }
        return nil
    }
}

class BaseListViewController: BaseViewController {
    lazy var containerStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 16
        return stackView
    }()
    
    private(set) lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.backgroundColor = .clear
        label.font = UIFont(name: "Manrope-Semibold", size: 14)
        label.textAlignment = .center
        return label
    }()
    
    private(set) lazy var topDividerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .lightGray
        return view
    }()
    
    private(set) lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        return tableView
    }()
    
    private(set) lazy var bottomDividerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .lightGray
        return view
    }()
    
    private(set) lazy var actionButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Button Title", for: .normal)
//        button.titleLabel?.font = UIFont(name: "Manrope-Semibold", size: 16)
        button.setTitleColor(.lightGray, for: .normal)
        button.backgroundColor = .black
        button.layer.cornerRadius = 24
        if #available(iOS 15.0, *) {
            var config = UIButton.Configuration.plain()
            config.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 48, bottom: 0, trailing: 48)
            button.configuration = config
        } else {
            button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 48, bottom: 0, right: 48)
        }
        button.addTarget(self, action: #selector(actionButtonTapped), for: .touchUpInside)
        return button
    }()
    
    // MARK: - Properties
    private var datasource: [ListableItem]?
    private var selectedDatasource: ListableItem?
    private var titleText: String?
    private var buttonTitleText: String?
    var actionButtonTap: ((ListableItem, BaseListViewController) -> Void)?
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    // MARK: - Setup
    private func setup() {
        self.setupUI()
    }
    
    private func setupUI() {
        self.view.backgroundColor = .white
        setupConstraints()
        setupTableView()
        setupTitles()
    }
    
    func setup(title: String, actionButtonTitle: String, datasource: [ListableItem]?) {
        self.titleText = title
        self.buttonTitleText = actionButtonTitle
        self.datasource = datasource
        tableView.reloadSafely()
    }
    
    private func setupTitles() {
        self.titleLabel.text = titleText
        self.actionButton.setTitle(buttonTitleText, for: .normal)
    }
    
    private func setupTableView() {
        tableView.register(BaseListCell.self, forCellReuseIdentifier: BaseListCell.identifier)
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    private func setupConstraints() {
        view.addSubview(containerStackView)
        containerStackView.addArrangedSubview(titleLabel)
        containerStackView.addArrangedSubview(topDividerView)
        containerStackView.addArrangedSubview(tableView)
        containerStackView.addArrangedSubview(bottomDividerView)
        
        let buttonContainer = UIView()
        buttonContainer.translatesAutoresizingMaskIntoConstraints = false
        buttonContainer.backgroundColor = .clear
        containerStackView.addArrangedSubview(buttonContainer)
        buttonContainer.addSubview(actionButton)
        
        NSLayoutConstraint.activate([
            containerStackView.topAnchor.constraint(equalTo: view.topAnchor, constant: 16),
            containerStackView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            containerStackView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            containerStackView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -16),
            topDividerView.heightAnchor.constraint(equalToConstant: 1),
            bottomDividerView.heightAnchor.constraint(equalToConstant: 1),
            actionButton.heightAnchor.constraint(equalToConstant: 48),
            actionButton.topAnchor.constraint(equalTo: buttonContainer.topAnchor),
            actionButton.centerXAnchor.constraint(equalTo: buttonContainer.centerXAnchor),
            actionButton.bottomAnchor.constraint(equalTo: buttonContainer.bottomAnchor),
        ])
    }
    
    // MARK: - Helpers
    
    // MARK: - Actions
    @objc private func actionButtonTapped() {
        if let selectedData = self.selectedDatasource {
            self.actionButtonTap?(selectedData, self)
        }
    }
}

// MARK: - UITableViewDataSource
extension BaseListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.datasource?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: BaseListCell.identifier, for: indexPath) as? BaseListCell,
              let data = self.datasource?[indexPath.row] else {
            return UITableViewCell()
        }
        cell.setup(with: data)
        return cell
    }
}

// MARK: - UITableViewDelegate
extension BaseListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        70
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let dataSource = datasource,
              indexPath.row < dataSource.count else {
            return
        }
        
        self.selectedDatasource = dataSource[indexPath.row]
        for (index) in dataSource.indices {
            self.datasource?[index].isSelected = index == indexPath.row
        }
        
        tableView.reloadSafely()
    }
}
