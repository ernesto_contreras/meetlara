//
//  BaseAlertViewController.swift
//  MeetLara
//
//  Created by Ernesto Jose Contreras Lopez on 30/10/23.
//

import UIKit
import AlamofireImage

class BaseAlertViewController: BaseViewController {
    // MARK: - Outlets
    @IBOutlet private(set) weak var containerView: UIView!
    @IBOutlet private(set) weak var signImageView: UIImageView!
    @IBOutlet private(set) weak var contentImageView: UIImageView!
    @IBOutlet private(set) weak var titleLabel: UILabel!
    @IBOutlet private(set) weak var codeLabel: UILabel!
    @IBOutlet private(set) weak var contentTextView: UITextView!
    @IBOutlet private(set) weak var messageHeight: NSLayoutConstraint!
    @IBOutlet private(set) weak var continueButton: UIButton!
    @IBOutlet private(set) weak var cancelButton: UIButton!
    
    // MARK: - Properties
    private var alertTitle: String?
    private var alertDescription: String?
    private var buttonTitle: String?
    private var image: String?
    private var contentImageURL: String?
    private var code: String?
    private var hasCancel: Bool = false
    private var attributedMessage: NSAttributedString?
    private var secondaryTitle: String?
    var canDismissTappingOutside: Bool = true
    var action: (() -> ())?
    var cancel: (() -> ())?
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let fixedWidth = contentTextView.frame.size.width
        let newSize = contentTextView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.leastNonzeroMagnitude))
        messageHeight.constant = newSize.height
    }
    
    // MARK: - Setup
    private func setup() {
        self.setupUI()
    }
    
    private func setupUI() {
        self.view.backgroundColor = .black.withAlphaComponent(0.5)
        contentTextView.textContainerInset.left = 8
        contentTextView.textContainerInset.right = 8
        populateUI()
        roundViews()
        setupGesture()
    }
    
    private func populateUI() {
        if let image = self.image {        
            self.signImageView.image = UIImage(named: image)
            self.signImageView.tintColor = .black
        }
        self.signImageView.isHidden = self.image.isNilOrEmpty
        self.contentTextView.linkTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.lightGray,
            NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        self.titleLabel.text = alertTitle
        self.titleLabel.isHidden = alertTitle.isNilOrEmpty
        if let attributedMessage = self.attributedMessage {
            self.contentTextView.attributedText = attributedMessage
        } else {
            self.contentTextView.text = alertDescription
        }
        self.continueButton.setTitle(buttonTitle, for: .normal)
        if let secondaryTitle = self.secondaryTitle {        
            self.cancelButton.setTitle(secondaryTitle, for: .normal)
        }
        if let contentURL = self.contentImageURL,
           let url = URL(string: contentURL) {
            self.contentImageView.af.setImage(withURL: url)
        }
        self.contentImageView.isHidden = self.contentImageURL.isNilOrEmpty
        
        self.codeLabel.text = self.code
        self.codeLabel.isHidden = self.code.isNilOrEmpty
        self.cancelButton.isHidden = !hasCancel
    }
    
    func setup(title: String, description: String, image: String, btnTitle: String, hasCancel: Bool) {
        self.alertTitle = title
        self.buttonTitle = btnTitle
        self.alertDescription = description
        self.image = image
        self.hasCancel = hasCancel
    }
    
    func attributeMessage(_ attributedMessage: NSAttributedString?) {
        self.attributedMessage = attributedMessage
    }
    
    func updateCancelTitle(_ title: String?) {
        self.secondaryTitle = title
    }
    
    private func roundViews() {
        containerView.layer.cornerRadius = 10
        signImageView.layer.cornerRadius = 10
        continueButton.layer.cornerRadius = 10
    }
    
    private func setupGesture() {
        guard canDismissTappingOutside else { return }
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleDissmiss(_:))))
    }
    
    // MARK: - Helpers
    
    // MARK: - Actions
    @objc private func handleDissmiss(_ gesture: UITapGestureRecognizer) {
        let tapLocation = gesture.location(in: self.view)
        let containerViewFrame = containerView.frame
        if !containerViewFrame.contains(tapLocation) {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction private func continueBtnTapped(_ sender: UIButton) {
        self.action?()
    }
    
    @IBAction private func cancelBtnTapped(_ sender: UIButton) {
        self.cancel?()
    }
}
