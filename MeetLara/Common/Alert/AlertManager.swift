//
//  AlertManager.swift
//  MeetLara
//
//  Created by Ernesto Jose Contreras Lopez on 30/10/23.
//

import UIKit

class AlertManager {
    static let shared = AlertManager()
    private init() {}
    private var rootVC: UIViewController? {
        var vc: UIViewController?
        for window in UIApplication.shared.windows {
            if window.isKeyWindow {
                vc = window.rootViewController
            }
        }
        return vc
    }
    
    // MARK: - Public
    public func presentAlert(isDismissable: Bool = false,
                             image: String? = nil,
                             title: String,
                             message: String,
                             attributedMessage: NSAttributedString? = nil,
                             buttonTitle: String,
                             hasCancel: Bool = false,
                             cancelTitle: String? = nil,
                             handler: (() -> Void)? = nil,
                             cancelHandler: (() -> Void)? = nil) {
        let alertVC = BaseAlertViewController(nibName: "BaseAlertViewController", bundle: nil)
        
        alertVC.setup(title: title,
                      description: message,
                      image: image ?? "",
                      btnTitle: buttonTitle,
                      hasCancel: hasCancel)
        alertVC.canDismissTappingOutside = isDismissable
        alertVC.attributeMessage(attributedMessage)
        alertVC.updateCancelTitle(cancelTitle)

        alertVC.modalPresentationStyle = .overFullScreen
        alertVC.modalTransitionStyle = .crossDissolve
        
        alertVC.action = {
            self.rootVC?.dismiss(animated: true) {
                handler?()
            }
        }
        
        alertVC.cancel = {
            self.rootVC?.dismiss(animated: true) {
                cancelHandler?()
            }
        }
        
        DispatchQueue.main.async {
            self.rootVC?.present(alertVC, animated: true, completion: nil)
        }
    }
    
    enum RedirectType {
        case presented
        case popable
        
        var message: String {
            switch self {
            case .popable:
                return "Regresar"
            case .presented:
                return "Cancelar"
            }
        }
    }
    
    public func displayPostError(redirection: RedirectType = .presented,
                                 isLastStep: Bool = false,
                                 with error: String?,
                                 handler: (() -> Void)? = nil,
                                 cancelHandler: (() -> Void)? = nil) {
        let msg = isLastStep ? "No fue posible emitir a través de la aseguradora seleccionada." : "No fue posible completar este proceso."
        
        self.displayErrorAlert(description: msg,
                               error: error ?? "",
                               btnTitle: "Reintentar",
                               hasCancel: true,
                               cancelTitle: redirection.message,
                               handler: handler) {
            switch redirection {
            case .popable:
                self.rootVC?.navigationController?.popViewController(animated: true)
            case .presented:
                cancelHandler?()
            }
        }
    }
    
    public func displayGetError(redirection: RedirectType = .presented,
                                with error: String?,
                                btnTitle: String = Localized.Texts.retry.string,
                                handler: (() -> Void)? = nil) {
        displayErrorAlert(description: Localized.API.Error.getError.string,
                          error: error ?? "",
                          btnTitle: btnTitle,
                          hasCancel: true,
                          cancelTitle: redirection.message,
                          handler: handler,
                          cancelHandler: {
            switch redirection {
            case .presented:
                break
            case .popable:
                self.rootVC?.navigationController?.popViewController(animated: true)
            }
        })
    }

    // MARK: - Private
    private func displayErrorAlert(title: String = Localized.API.Error.defaultError.string,
                                   description: String,
                                   error: String,
                                   btnTitle: String = Localized.Texts.accept.string,
                                   hasCancel: Bool = false,
                                   cancelTitle: String? = nil,
                                   handler: (() -> Void)? = nil,
                                   cancelHandler: (() -> Void)? = nil) {
        self.presentAlert(image: Localized.Images.check_circle.name,
                          title: title,
                          message: description,
                          buttonTitle: btnTitle,
                          hasCancel: hasCancel,
                          cancelTitle: cancelTitle,
                          handler: handler,
                          cancelHandler: cancelHandler)
    }

    private func showLogOutAlert() {
        
    }
}
