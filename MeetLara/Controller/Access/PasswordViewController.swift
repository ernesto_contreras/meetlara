//
//  PasswordViewController.swift
//  MeetLara
//
//  Created by Ernesto Jose Contreras Lopez on 3/11/23.
//

import UIKit

class PasswordViewController: BaseViewController {
    // MARK: - Properties
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.text = Localized.Texts.setupPassword.string
        label.font = UIFont.systemFont(ofSize: 26, weight: .bold)
        label.textColor = Localized.Colors.primaryText.color
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    private lazy var vStackView = {
        BaseStackView(space: 40, orientation: .vertical)
    }()
    private lazy var imageHStackView: BaseStackView = {
        BaseStackView(space: 12, orientation: .horizontal)
    }()
    private lazy var logoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.backgroundColor = Localized.Colors.contrast.color
        imageView.layer.cornerRadius = Localized.Sizes.imageLogo.size / 2
        return imageView
    }()
    private lazy var passwordForm: BaseForm = {
       let form = BaseForm()
        form.setup(title: Localized.Texts.passwordTitle.string,
                   placeholder: Localized.Texts.typePassword.string,
                   type: .password,
                   hasPasswordChecklist: true)
        return form
        
    }()
    private lazy var confirmPasswordForm: BaseForm = {
       let form = BaseForm()
        form.setup(title: Localized.Texts.confirmPasswordTitle.string,
                   placeholder: Localized.Texts.typeConfirmPassword.string,
                   type: .password,
                   hasPasswordCheck: true)
        return form
    }()
    private lazy var registerButton: BaseButton = {
        BaseButton(title: Localized.Texts.registerButton.string,
                   target: self,
                   action: #selector(registerUser))
    }()
    private var user: User!

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupObservers()
        setupUI()
    }
    
    // MARK: - Setup
    public func setup(user: User) {
        self.user = user
    }
    
    private func setupObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(handleFormStartEditing(_:)), name: .didStartEditingForm, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleFormUpdate(_:)), name: .didUpdateForm, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleFormFinishEditing(_:)), name: .didFinishEditingForm, object: nil)
    }
    
    private func setupUI() {
        contentView.addSubview(vStackView)
        
        vStackView.addArrangedSubview(imageHStackView)
        imageHStackView.addArrangedSubview(logoImageView)
        let titleContainer = UIView()
        titleContainer.addSubview(titleLabel)
        imageHStackView.addArrangedSubview(titleContainer)
        
        vStackView.addArrangedSubview(BaseStackView(space: 12,
                                                    orientation: .vertical,
                                                    subViews: [passwordForm, confirmPasswordForm]))

        vStackView.addArrangedSubview(registerButton)
        
        NSLayoutConstraint.activate([
            contentView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1),
            logoImageView.widthAnchor.constraint(equalToConstant: Localized.Sizes.imageLogo.size),
            logoImageView.heightAnchor.constraint(equalToConstant: Localized.Sizes.imageLogo.size),
            titleLabel.leadingAnchor.constraint(equalTo: titleContainer.leadingAnchor, constant: 16),
            titleLabel.trailingAnchor.constraint(equalTo: titleContainer.trailingAnchor, constant: -16),
            titleLabel.centerYAnchor.constraint(equalTo: titleContainer.centerYAnchor),
            vStackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            vStackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
            vStackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 32),
            vStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -60)
        ])
        
        registerButton.enable(should: hasValidInputs())
    }
    
    // MARK: - Helpers
    private func hasValidInputs() -> Bool {
        passwordForm.isValid() && confirmPasswordForm.isValid()
    }
    
    // MARK: - Actions
    @objc private func registerUser() {
        registerButton.showLoading()
        FirebaseManager.shared.registerUser(user,
                                           withPwd: passwordForm.content()) { error in
            self.registerButton.hideLoading()
            if let error = error?.message {
                print("T3ST error", error)
            }
        }
    }
    
    @objc private func handleFormStartEditing(_ notification: Notification) {
        selectedView = registerButton
    }
    
    @objc private func handleFormUpdate(_ notification: Notification) {
        guard let focusedTextField = notification.object as? UITextField else { return }
        let hasMinLenght = passwordForm.content().hasMinimunLenght()
        let hasUppercase = passwordForm.content().hasUppercase()
        let hasNumber = passwordForm.content().hasNumber()
        let hasSpecialCharacter = passwordForm.content().hasSpecialCharacter()
        let validationsMatch = hasMinLenght && hasUppercase && hasNumber && hasSpecialCharacter
        let validation = passwordForm.content() == self.confirmPasswordForm.content() && validationsMatch

        if focusedTextField == passwordForm.textField {
            passwordForm.updatePasswordValidation(.minLenght, validation: hasMinLenght)
            passwordForm.updatePasswordValidation(.uppercase, validation: hasUppercase)
            passwordForm.updatePasswordValidation(.number, validation: hasNumber)
            passwordForm.updatePasswordValidation(.specialCharacter, validation: hasSpecialCharacter)
        } else {
            confirmPasswordForm.updatePasswordValidation(.passwordsMatch, validation: validation)
        }

        registerButton.enable(should: validation)
    }
    
    @objc private func handleFormFinishEditing(_ notification: Notification) {
        selectedView = nil
        if passwordForm.content().isEmpty {
            passwordForm.resetPasswordValidations()
            confirmPasswordForm.resetPasswordValidations()
            confirmPasswordForm.updateText(nil)
        }
    }
}

