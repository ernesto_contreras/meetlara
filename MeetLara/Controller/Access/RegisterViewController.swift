//
//  RegisterViewController.swift
//  MeetLara
//
//  Created by Ernesto Jose Contreras Lopez on 2/11/23.
//

import UIKit

class RegisterViewController: BaseViewController {
    // MARK: - Properties
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.text = Localized.Texts.personalInfo.string
        label.font = UIFont.systemFont(ofSize: 26, weight: .bold)
        label.textColor = Localized.Colors.primaryText.color
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    private lazy var vStackView = {
        BaseStackView(space: 40, orientation: .vertical)
    }()
    private lazy var imageHStackView: BaseStackView = {
        BaseStackView(space: 12, orientation: .horizontal)
    }()
    private lazy var logoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.backgroundColor = Localized.Colors.contrast.color
        imageView.layer.cornerRadius = Localized.Sizes.imageLogo.size / 2
        return imageView
    }()
    private lazy var fullNameForm: BaseForm = {
        let form = BaseForm()
        form.setup(title: Localized.Texts.nameTitle.string,
                   placeholder: Localized.Texts.typeName.string,
                   error: Localized.Errors.typeNameError.string,
                   type: .regular)
        return form
    }()
    private lazy var emailForm: BaseForm = {
        let form = BaseForm()
        form.setup(title: Localized.Texts.emailTitle.string,
                   placeholder: Localized.Texts.typeEmail.string,
                   error: Localized.Errors.typeEmailError.string,
                   type: .email)
        return form
    }()
    private lazy var instagramForm: BaseForm = {
        let form = BaseForm()
        form.setup(title: Localized.Texts.instagramTitle.string,
                   placeholder: Localized.Texts.typeInstagram.string,
                   error: Localized.Errors.typeInstagramError.string,
                   type: .regular)
        return form
    }()
    private lazy var continueButton: BaseButton = {
        BaseButton(title: Localized.Texts.continueButton.string,
                   target: self,
                   action: #selector(registerUser))
    }()

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupObservers()
        setupUI()
    }
    
    // MARK: - Setup
    private func setupObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(handleFormStartEditing(_:)), name: .didStartEditingForm, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleFormUpdate(_:)), name: .didUpdateForm, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleFormFinishEditing(_:)), name: .didFinishEditingForm, object: nil)
    }
    
    private func setupUI() {
        contentView.addSubview(vStackView)
        
        vStackView.addArrangedSubview(imageHStackView)
        imageHStackView.addArrangedSubview(logoImageView)
        let titleContainer = UIView()
        titleContainer.addSubview(titleLabel)
        imageHStackView.addArrangedSubview(titleContainer)
        
        vStackView.addArrangedSubview(BaseStackView(space: 12,
                                                    orientation: .vertical,
                                                    subViews: [fullNameForm, emailForm, instagramForm]))

        vStackView.addArrangedSubview(continueButton)
        
        NSLayoutConstraint.activate([
            contentView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1),
            logoImageView.widthAnchor.constraint(equalToConstant: Localized.Sizes.imageLogo.size),
            logoImageView.heightAnchor.constraint(equalToConstant: Localized.Sizes.imageLogo.size),
            titleLabel.leadingAnchor.constraint(equalTo: titleContainer.leadingAnchor, constant: 16),
            titleLabel.trailingAnchor.constraint(equalTo: titleContainer.trailingAnchor, constant: -16),
            titleLabel.centerYAnchor.constraint(equalTo: titleContainer.centerYAnchor),
            vStackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            vStackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
            vStackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 32),
            vStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -60)
        ])
        
        continueButton.enable(should: hasValidInputs())
    }
    
    // MARK: - Helpers
    private func hasValidInputs() -> Bool {
        fullNameForm.isValid() && emailForm.isValid() && instagramForm.isValid()
    }
    
    // MARK: - Actions
    @objc private func registerUser() {
        let passwordVC = PasswordViewController()
        passwordVC.setup(
            user: User(
                full_name: fullNameForm.content(),
                email: emailForm.content(),
                instagram: instagramForm.content()
            )
        )
        pushSafely(vc: passwordVC)
    }
    
    @objc private func handleFormStartEditing(_ notification: Notification) {
        selectedView = continueButton
    }
    
    @objc private func handleFormUpdate(_ notification: Notification) {
        continueButton.enable(should: hasValidInputs())
    }
    
    @objc private func handleFormFinishEditing(_ notification: Notification) {
        selectedView = nil
    }
}
