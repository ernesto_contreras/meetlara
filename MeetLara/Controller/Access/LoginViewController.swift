//
//  LoginViewController.swift
//  MeetLara
//
//  Created by Ernesto Jose Contreras Lopez on 30/10/23.
//

import UIKit
import FirebaseAuth

class LoginViewController: BaseViewController {
    // MARK: - Properties
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.text = Localized.Texts.welcome.string
        label.font = UIFont.systemFont(ofSize: 26, weight: .bold)
        label.textColor = Localized.Colors.primaryText.color
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    private lazy var vStackView: BaseStackView = {
        BaseStackView(space: 40, orientation: .vertical)
    }()
    private lazy var imageHStackView: BaseStackView = {
        BaseStackView(space: 12, orientation: .horizontal)
    }()
    private lazy var logoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.backgroundColor = Localized.Colors.contrast.color
        imageView.layer.cornerRadius = Localized.Sizes.imageLogo.size / 2
        return imageView
    }()
    private lazy var emailForm: BaseForm = {
        let form = BaseForm()
        form.setup(title: Localized.Texts.emailTitle.string,
                   placeholder: Localized.Texts.typeEmail.string,
                   error: Localized.Errors.typeEmailError.string,
                   type: .email)
        return form
    }()
    private lazy var passwordForm: BaseForm = {
       let form = BaseForm()
        form.setup(title: Localized.Texts.passwordTitle.string,
                   placeholder: Localized.Texts.typePassword.string,
                   error: Localized.Errors.typePasswordError.string,
                   type: .password)
        return form
    }()
    private lazy var loginButton: BaseButton = {
        BaseButton(title: Localized.Texts.loginButton.string,
                   target: self,
                   action: #selector(loginUser))
    }()
    private lazy var registerButton: BaseButton = {
        BaseButton(type: .outlined,
                   title: Localized.Texts.registerButton.string,
                   target: self,
                   action: #selector(registerUser))
    }()

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupObservers()
        setupUI()
    }
    
    // MARK: - Setup
    private func setupObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(handleFormStartEditing(_:)),
                                               name: .didStartEditingForm, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleFormUpdate(_:)),
                                               name: .didUpdateForm, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleFormFinishEditing(_:)),
                                               name: .didFinishEditingForm, object: nil)
    }
    
    private func setupUI() {
        contentView.addSubview(vStackView)
        vStackView.addArrangedSubview(imageHStackView)
        imageHStackView.addArrangedSubview(logoImageView)
        let titleContainer = UIView()
        titleContainer.addSubview(titleLabel)
        imageHStackView.addArrangedSubview(titleContainer)
        
        vStackView.addArrangedSubview(BaseStackView(space: 12,
                                                    orientation: .vertical,
                                                    subViews: [emailForm, passwordForm]))

        vStackView.addArrangedSubview(BaseStackView(space: 12,
                                                    orientation: .vertical,
                                                    subViews: [loginButton, registerButton]))
        
        NSLayoutConstraint.activate([
            contentView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1),
            logoImageView.widthAnchor.constraint(equalToConstant: Localized.Sizes.imageLogo.size),
            logoImageView.heightAnchor.constraint(equalToConstant: Localized.Sizes.imageLogo.size),
            titleLabel.leadingAnchor.constraint(equalTo: titleContainer.leadingAnchor, constant: 16),
            titleLabel.trailingAnchor.constraint(equalTo: titleContainer.trailingAnchor, constant: -16),
            titleLabel.centerYAnchor.constraint(equalTo: titleContainer.centerYAnchor),
            vStackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            vStackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
            vStackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 32),
            vStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -60)
        ])
        
        loginButton.enable(should: hasValidInputs())
    }
    
    // MARK: - Helpers
    private func hasValidInputs() -> Bool {
        emailForm.isValid() && passwordForm.isValid()
    }
    
    // MARK: - Actions
    @objc private func loginUser() {
        loginButton.showLoading()
        FirebaseManager.shared.loginUser(withEmail: emailForm.content(),
                                        andPwd: passwordForm.content()) { error in
            self.loginButton.hideLoading()
            if let error = error?.message {
                print("T3ST error", error)
            } else {
                
            }
        }
    }
    
    @objc private func registerUser() {
        let registerVC = RegisterViewController()
        navigationController?.pushViewController(registerVC, animated: true)
    }
    
    @objc private func handleFormStartEditing(_ notification: Notification) {
        selectedView = loginButton
    }
    
    @objc private func handleFormUpdate(_ notification: Notification) {
        loginButton.enable(should: hasValidInputs())
    }
    
    @objc private func handleFormFinishEditing(_ notification: Notification) {
        selectedView = nil
    }
}
