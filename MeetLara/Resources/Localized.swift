//
//  Localized.swift
//  MeetLara
//
//  Created by Ernesto Jose Contreras Lopez on 31/10/23.
//

import UIKit

class Localized {
    // MARK: - Colors localization.
    enum Colors {
        case primary
        case secondary
        case auxiliar
        case primaryText
        case secondaryText
        case contrast
        
        var color: UIColor {
            switch self {
            case .primary:
                return UIColor(named: "Primary")!
            case .secondary:
                return UIColor(named: "Secondary")!
            case .auxiliar:
                return UIColor(named: "Auxiliar")!
            case .primaryText:
                return UIColor(named: "PrimaryText")!
            case .secondaryText:
                return UIColor(named: "SecondaryText")!
            case .contrast:
                return UIColor(named: "Contrast")!
            }
        }
    }
    
    // MARK: - Images localization.
    enum Images: String {
        case eye_icon
        case eye_off_icon
        case check_circle
        case loading_indicator
        case unchecked_square
        case radio_off
        case radio_on
        case alert_icon
        
        var image: UIImage {
            UIImage(named: self.rawValue)!
        }
        
        var imageView: UIImageView {
            if let originalImage = UIImage(named: self.rawValue) {
                let tintedImage = originalImage.withRenderingMode(.alwaysTemplate)
                let imageView = UIImageView(image: tintedImage)
                imageView.tintColor = Localized.Colors.contrast.color
                return imageView
            }
            return UIImageView()
        }
        
        var name: String {
            self.rawValue
        }
    }
    
    // MARK: - System images localization.
    enum SystemImages: String {
        case xmark_circle_fill
        
        var image: UIImage {
            UIImage(systemName: self.rawValue.replacingOccurrences(of: "_", with: "."))!
        }
    }
    
    enum Texts: String {
        case nameTitle            = "Nombre completo"
        case typeName             = "Ingresa tu nombre y apellido"
        case emailTitle           = "Correo electrónico"
        case typeEmail            = "Ingresa tu correo electrónico"
        case instagramTitle       = "Instagram"
        case typeInstagram        = "Ingresa tu usuario de Instagram"
        case passwordTitle        = "Contraseña"
        case typePassword         = "Ingresa tu contraseña"
        case confirmPasswordTitle = "Confirmar contraseña"
        case typeConfirmPassword  = "Ingresa tu contraseña nuevamente"
        case loginButton          = "Iniciar sesión"
        case registerButton       = "Registrarme"
        case registerTitle        = "¿No tienes cuenta?"
        case forgotPassword       = "¿Olvidaste tu contraseña?"
        case loadingMessage       = "Espera un momento, estamos descargando toda tu información."
        case retry                = "Reintentar"
        case cancel               = "Cancelar"
        case accept               = "Aceptar"
        case continueButton       = "Continuar"
        case welcome              = "¡Bienvenido!"
        case personalInfo         = "Información personal"
        case setupPassword        = "Información de acceso"
        case lengthLimit          = "Al menos 8 caracteres"
        case numberLimit          = "Al menos un número"
        case characterLimit       = "Al menos un carácter especial"
        case capitalLimit         = "Al menos una letra mayúscula"
        case matchPasswords       = "Ambas contraseñas son iguales"
        
        var string: String {
            self.rawValue
        }
    }
    
    enum Errors: String {
        case typeNameError        = "Debes ingresar un nombre válido"
        case typeLastNameError    = "Debes ingresar un apellido válido"
        case typeEmailError       = "Debes ingresar un correo válido"
        case typeInstagramError   = "Debes ingresar un usuario de Instagram válido"
        case typePasswordError    = "Debes ingresar una contraseña válida"
        case confirmPasswordError = "Las contraseñas no coinciden"
        
        var string: String {
            self.rawValue
        }
    }
    
    enum Sizes {
        case imageLogo
        case imageLogoSmall
        
        var size: CGFloat {
            switch self {
            case .imageLogo:
                return UIScreen.main.bounds.width / 2.5
            case .imageLogoSmall:
                return UIScreen.main.bounds.width / 4
            }
        }
    }
    
    struct API {
        static let baseURL = "https://meetlara-bqto-default-rtdb.firebaseio.com/"
        
        enum Endpoint: String {
            case users = "users/"
            
            var string: String {
                self.rawValue
            }
        }
        
        enum Error: String {
            case defaultError         = "Ha ocurrido un error"
            case getError             = "No se pudo obtener la información"
            case postError            = "No se pudo enviar la información"
            case loginError           = "No se pudo iniciar sesión"
            case registerError        = "No se pudo registrar el usuario"
            case createAccountError   = "No se pudo crear la cuenta"
            case passwordError        = "No se pudo actualizar la contraseña"
            case wrongDecoding        = "No se pudo decodificar la información"
            case tokenError           = "No se pudo obtener el token"
            case badURL               = "URL inválida"
            
            var string: String {
                self.rawValue
            }
            
            var code: Int {
                switch self {
                case .defaultError, .createAccountError, .registerError, .passwordError, .tokenError, .loginError:
                    return 600
                case .wrongDecoding:
                    return 610
                case .badURL:
                    return 620
                case .getError:
                    return 630
                case .postError:
                    return 640
                }
            }
            
            func extraMessage(_ message: String) -> String {
                "\(self.rawValue) \(message)"
            }
        }
        
    }
}

// MARK: - Notification names
extension Notification.Name {
    static let didUpdateForm = Notification.Name("didUpdateForm")
    static let didFinishEditingForm = Notification.Name("didFinishEditingForm")
    static let didStartEditingForm = Notification.Name("didStartEditingForm")
}
