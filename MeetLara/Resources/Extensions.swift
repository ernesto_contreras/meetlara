//
//  Extensions.swift
//  MeetLara
//
//  Created by Ernesto Jose Contreras Lopez on 30/10/23.
//

import UIKit

protocol DictionaryConvertible: Codable {
    func toDictionary() -> [String: Any]
}

extension DictionaryConvertible {
    func toDictionary() -> [String: Any] {
        let encoder = JSONEncoder()
        encoder.keyEncodingStrategy = .convertToSnakeCase
        let data = try? encoder.encode(self)
        let dictionary = try? JSONSerialization.jsonObject(with: data ?? Data(), options: []) as? [String: Any]
        return dictionary ?? [:]
    }
}

extension UIViewController {
    func presentNavigationVC<T: UIViewController>(vc: T, style: UIModalPresentationStyle = .formSheet) {
        let viewController =  UINavigationController(rootViewController: vc)
        viewController.modalPresentationStyle = style
        DispatchQueue.main.async {
            self.present(viewController, animated: true)
        }
    }
    
    @objc func pushSafely(vc: UIViewController, animated: Bool = true) {
        DispatchQueue.main.async {
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension UIView {
    func pinToEdges(of view: UIView) {
        translatesAutoresizingMaskIntoConstraints = false
        topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
    }
    
    func enable(should: Bool) {
        self.isUserInteractionEnabled = should
        self.alpha = should ? 1 : 0.5
    }

    func disable(should: Bool) {
        self.isUserInteractionEnabled = !should
        self.alpha = !should ? 1 : 0.5
    }
}

enum DateType: String {
    case dayMonthYearDashed = "dd-MM-yyyy"
    case dayMonthYearShort = "dd MMM yyyy"
    case yearMonthDayDashed = "yyyy-MM-dd"
    case yearMonthDayDashedWithFulltime = "yyyy-MM-dd HH:mm:ss"
    case dayMonthYearLong = "dd MMMM yyyy"
}

extension String {
    func alphaNumericOnly() -> Bool {
        let allowedCharacters = CharacterSet.alphanumerics
        let characterSet = CharacterSet(charactersIn: self)
        return allowedCharacters.isSuperset(of: characterSet)
    }
    
    func numbersOnly() -> Bool {
        let allowedCharacters = CharacterSet(charactersIn:"0123456789")
        let characterSet = CharacterSet(charactersIn: self)
        return allowedCharacters.isSuperset(of: characterSet)
    }
    
    func formattedPath() -> String {
        var newPath = self

        // Ensure single / before api
        if newPath.hasPrefix("/") {
            newPath.removeFirst()
        }

        return newPath
    }
    
    func toPrice() -> String? {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.maximumFractionDigits = 2
        formatter.locale = Locale.current
        formatter.groupingSeparator = ","
        formatter.decimalSeparator = "."
        let number = Float(self) ?? 0.0
        if let price = formatter.string(from: NSNumber(value: number)) {
            return "$\(price)"
        } else {
            return nil
        }
    }
    
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let predicate = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return predicate.evaluate(with: self)
    }
    
    func hasMinimunLenght() -> Bool {
        self.count >= 8
    }
    
    func hasUppercase() -> Bool {
        let uppercaseLetterRegex = ".*[A-Z]+.*"
        return NSPredicate(format: "SELF MATCHES %@", uppercaseLetterRegex).evaluate(with: self)
    }
    
    func hasSpecialCharacter() -> Bool {
        let specialCharacterRegex = ".*[^A-Za-z0-9]+.*"
        return NSPredicate(format: "SELF MATCHES %@", specialCharacterRegex).evaluate(with: self)
    }
    
    func hasNumber() -> Bool {
        let numberRegex = ".*[0-9]+.*"
        return NSPredicate(format: "SELF MATCHES %@", numberRegex).evaluate(with: self)
    }
    
    func convertDateformat(from type: DateType,
                           to format: DateType) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = type.rawValue
        
        if let date = dateFormatter.date(from: self) {
            dateFormatter.dateFormat = format.rawValue
            dateFormatter.locale = Locale(identifier: "es_ES")
            return dateFormatter.string(from: date).capitalized
        }
        
        return nil
    }
}

extension UITextField {
    @objc func limited(to characters: Int, range: NSRange, string: String) -> Bool {
        let currentText = self.text ?? ""
        let newLength = (currentText as NSString).length + string.count - range.length
        
        return newLength <= characters
    }
    
    func getUpdatedText(from range: NSRange, with string: String) -> String? {
        (self.text as? NSString)?.replacingCharacters(in: range, with: string)
    }
}

extension UIScrollView {
    func resetInsets() {
        let contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.contentInset = contentInsets
        self.scrollIndicatorInsets = contentInsets
    }
}

extension Date {
    static func todayAsString(isOneYearLater: Bool = false) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "es_ES")
        dateFormatter.dateFormat = "dd-MM-yyyy"
        var date: Date
        if isOneYearLater {
            date = Calendar.current.date(byAdding: .year, value: 1, to: Date()) ?? Date()
        } else {
            date = Date()
        }
        
        return dateFormatter.string(from: date)
    }
}

extension Optional where Wrapped: Collection {
    var isNilOrEmpty: Bool {
        return self?.isEmpty ?? true
    }
}

extension UITableView {
    func reloadSafely(animated: Bool = false) {
        DispatchQueue.main.async {
            if animated {
                self.reloadSections(IndexSet(integer: 0), with: .automatic)
            } else {
                self.reloadData()
            }
        }
    }
    
    func showEmptyView(withMessage message: String = "¡Sin información para mostrar!") {
        let emptyView = UIView(frame: bounds)
        let imageView = Localized.Images.alert_icon.imageView
        imageView.tintColor = .black
        imageView.translatesAutoresizingMaskIntoConstraints = false
        let messageLabel = UILabel()
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        messageLabel.text = message
        messageLabel.textAlignment = .center
        messageLabel.textColor = .black
        messageLabel.numberOfLines = 0
        emptyView.addSubview(imageView)
        emptyView.addSubview(messageLabel)
        NSLayoutConstraint.activate([
            imageView.centerYAnchor.constraint(equalTo: emptyView.centerYAnchor, constant: -48),
            imageView.centerXAnchor.constraint(equalTo: emptyView.centerXAnchor),
            imageView.widthAnchor.constraint(equalToConstant: 80),
            imageView.heightAnchor.constraint(equalToConstant: 80),
            messageLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 16),
            messageLabel.centerXAnchor.constraint(equalTo: emptyView.centerXAnchor),
            messageLabel.trailingAnchor.constraint(equalTo: emptyView.trailingAnchor, constant: -48),
            messageLabel.leadingAnchor.constraint(equalTo: emptyView.leadingAnchor, constant: 48)
        ])
        
        backgroundView = emptyView
    }
    
    func hideEmptyView() {
        backgroundView = nil
    }
}

extension UICollectionView {
    func reloadSafely() {
        DispatchQueue.main.async {
            self.reloadData()
        }
    }
}

extension UISearchBar {
    func getTextField() -> UITextField? {
        return value(forKey: "searchField") as? UITextField
    }
    
    func setTextField(color: UIColor?) {
        guard let textField = getTextField() else { return }
        switch searchBarStyle {
        case .minimal:
            textField.layer.backgroundColor = color?.cgColor
            textField.layer.cornerRadius = 6
        case .prominent, .default:
            textField.backgroundColor = color
        @unknown default:
            break
        }
    }
}
