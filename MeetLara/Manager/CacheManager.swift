//
//  CacheManager.swift
//  MeetLara
//
//  Created by Ernesto Jose Contreras Lopez on 30/10/23.
//

import Foundation

class CacheManager {
    static let shared = CacheManager()
    private let localStorage = UserDefaults.standard
    private init() {}
    
    public enum LocalID: String {
        case user
    }
    
    public func save<T: Codable>(_ value: T, key: LocalID) {
        UserDefaults.standard.set(
            try? PropertyListEncoder().encode(value), forKey: key.rawValue
        )
    }
    
    public func remove(key: LocalID) {
        UserDefaults.standard.removeObject(forKey: key.rawValue)
    }
    
    public func save(value: Any, at key: LocalID) {
        localStorage.set(value, forKey: key.rawValue)
    }
    
    public func get(valueAt key: LocalID) -> Any? {
        localStorage.value(forKey: key.rawValue)
    }
    
    public func getObject<T: Codable>(key: LocalID) -> T? {
        var userData: T?
        if let data = UserDefaults.standard.value(forKey: key.rawValue) as? Data {
            userData = try? PropertyListDecoder().decode(T.self, from: data)
        }
        return userData
    }
}
