//
//  FirebaseManager.swift
//  MeetLara
//
//  Created by Ernesto Jose Contreras Lopez on 4/11/23.
//

import FirebaseDatabase
import FirebaseAuth

class FirebaseManager {
    static let shared = FirebaseManager()
    private var ref = Database.database().reference()
    public var currentUserID: String? {
        Auth.auth().currentUser?.uid
    }
    private init() {}
    
    public func loginUser(withEmail email: String,
                   andPwd password: String,
                   completion: @escaping((BaseError?) -> Void)) {
        Auth.auth().signIn(withEmail: email, password: password) { signInResult, error in
            if let error = error {
                completion(BaseError(error: .loginError, message: error.localizedDescription))
            }
            
            if let signInResult = signInResult {
                self.getObject(User.self, withID: signInResult.user.uid, inCollection: .users) { result in
                    switch result {
                    case .success(let user):
                        CacheManager.shared.save(user, key: .user)
                        completion(nil)
                    case .failure(let error):
                        completion(error)
                    }
                }
            }
        }
    }
    
    public func registerUser(_ user: User,
                      withPwd password: String,
                      completion: @escaping((BaseError?) -> Void)) {
        Auth.auth().createUser(withEmail: user.email, password: password) { result, error in
            if let error = error {
                completion(BaseError(error: .registerError, message: error.localizedDescription))
            }
            
            if let result = result {
                self.createObject(user, withID: result.user.uid, inCollection: .users) { error in
                    completion(error)
                }
            }
        }
    }
    
    public func logoutUser() {
        do {
            try Auth.auth().signOut()
            CacheManager.shared.remove(key: .user)
            SceneSelector.shared.setInitialScene()
        } catch {
            print("T3ST logoutUser", error.localizedDescription)
        }
    }
    
    public func createObject<T: DictionaryConvertible>(_ object: T,
                                                       withID id: String,
                                                      inCollection collection: CollectionKey,
                                                      completion: @escaping((BaseError?) -> Void)) {
        ref.child(collection.rawValue).child(id).setValue(object.toDictionary()) { error, _ in
            if let error = error {
                completion(BaseError(error: .defaultError, message: error.localizedDescription))
            }
            
            completion(nil)
        }
    }

    public func getObject<T: DictionaryConvertible>(_ type: T.Type,
                                                    withID id: String,
                                                    inCollection collection: CollectionKey,
                                                    completion: @escaping((Result<T?, BaseError>) -> Void)) {
        ref.child(collection.rawValue).child(id).observeSingleEvent(of: .value) { snapshot, arg  in
            guard let objectDict = snapshot.value as? [String: Any] else {
                completion(.failure(BaseError(error: .loginError, message: arg ?? "")))
                return
            }
            
            do {
                let data = try JSONSerialization.data(withJSONObject: objectDict)
                let object = try JSONDecoder().decode(type, from: data)
                completion(.success(object))
            } catch {
                completion(.failure(BaseError(error: .wrongDecoding, message: error.localizedDescription)))
            }
        }
    }
    
    public enum CollectionKey: String {
        case users
        case places
    }
}
