//
//  SceneSelector.swift
//  MeetLara
//
//  Created by Ernesto Jose Contreras Lopez on 30/10/23.
//

import UIKit

class SceneSelector {
    static let shared = SceneSelector()
    private init() {}

    func setInitialScene() {
        self.setLoginScene()
    }
    
    func setLoginScene() {
        let loginVC = LoginViewController()
        setScene(from: loginVC, withNavigation: true)
    }

    func setScene(from viewController: UIViewController,
                  withNavigation navigation: Bool = false) {

        let nextViewController: UIViewController

        if navigation {
            nextViewController = UINavigationController(rootViewController: viewController)
        } else {
            nextViewController = viewController
        }

        setViewControllerInKeyWindow(nextViewController)
    }

    func setViewControllerInKeyWindow(_ viewController: UIViewController) {

        guard let window = UIApplication.shared.connectedScenes.compactMap({$0 as? UIWindowScene}).flatMap({$0.windows}).first(where: { $0.isKeyWindow }) else {
            return
        }

        for subview in window.subviews {
            subview.removeFromSuperview()
        }

        let transition = CATransition()
        transition.duration = 1
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = .fade
        transition.fillMode = .both
        window.layer.add(transition, forKey: nil)

        window.rootViewController = viewController
    }
}
